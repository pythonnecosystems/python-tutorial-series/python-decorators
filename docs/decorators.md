# Python 데코레이터: 함수와 클래스 데코레이터

## <a name="intro"></a> 개요
Python 데코레이터에 관한 이 포스팅에서는 다음과 같은 내용을 배울 수 있다.

- Python에서 데코레이터란 무엇이며 어떻게 작동하는가?
- 함수의 동작을 수정하거나 향상시키기 위해 함수 데코레이터를 작성하고 사용하는 방법
- 클래스 데코레이터를 작성하고 사용하여 클래스의 동작을 수정하거나 향상시키는 방법
- Python 프로그래밍에서 데코레이터의 일반적인 사용 사례와 예

이 글을 읽고나면 Python에서 데코레이터의 개념과 응용에 대해 확실하게 이해할 수 있을 것이다.

하지만 자세한 내용을 살펴보기 전에 간단한 질문부터 시작하겠다. Python에서 데코레이터란 무엇일까?

## <a name="sec_02"></a> Python에서 데코레이터란?
Python의 데코레이터는 다른 함수를 인수로 받아 그 함수의 수정된 버전을 반환하는 특수한 종류의 함수이다. 수정된 함수는 로깅, 캐싱, 타이밍, 유효성 검사 등 추가적인 기능이나 변경된 기능을 가질 수 있다. 원래 함수는 변경되지 않았지만 데코레이터는 함수를 호출할 때마다 수정 사항을 적용할 수 있도록 해준다.

데코레이터는 동일한 코드를 반복할 필요 없이 여러 함수에 걸쳐 공통 기능을 재사용하고 싶을 때 유용하다. 예를 들어, 여러분이 프로그램에서 여러 함수의 실행 시간을 측정하고 싶다고 가정해 보겠다. 각 함수의 타이밍을 위해 동일한 코드를 작성하는 대신, 우리를 위해 타이밍을 수행하는 데코레이터를 작성하여 원하는 함수에 적용할 수 있다.

데코레이터는 또한 생성기, 코루틴, 컨텍스트 관리자 등과 같은 Python의 일부 고급 기능을 구현하기 위한 강력한 도구이다. 데코레이터는 싱글톤(singleton), 관찰자(observer), 프록시(proxy) 등 일부 디자인 패턴을 구현하는 데에도 사용될 수 있다.

Python에서 데코레이터는 `@` 기호에 이어 데코레이터 함수의 이름을 사용하여 구현된다. 예를 들어, `timer`라는 이름의 데코레이터 함수가 있다면, 다음과 같이 프로그램하여 `foo`라는 이름의 다른 함수에 적용할 수 있다.

```python
@timer
def foo():
    # some code
```

이는 다음 코드와 같다.

```python
def foo():
    # some code
foo = timer(foo)
```

보다시피 데코레이터 함수는 원래 함수를 인수로 받아 수정된 함수를 반환하고, 수정된 함수는 원래 함수와 동일한 이름으로 할당된다. 이런 식으로 우리는 `foo`를 부를 때마다 실제로 데코레이터의 추가 기능을 가진 수정된 함수를 호출하는 것이다.

[다음 절](#sec_02)에서는 Python에서 함수 데코레이터를 작성하고 사용하는 방법에 대해 알아보겠다.

## <a name="sec_03"></a> 함수 데코레이터 작성과 사용 방법
이 절에서는 Python에서 함수 데코레이터를 작성하고 사용하는 방법에 대해 알아본다. 함수 데코레이터는 다른 함수를 인수로 받아 수정된 함수를 반환하는 함수이다. 수정된 함수는 로깅, 캐싱, 타이밍, 유효성 검사 등 추가적이거나 변경된 기능을 가질 수 있다.

함수 데코레이터를 작성하려면 다음 단계를 따라야 한다.

1. 원래 함수와 동일한 인수를 사용하고 추가 또는 변경된 함수를 수행하는 래퍼 함수를 정의한다.
1. 래퍼 함수 안에 있는 원래 함수을 호출하고 결과를 반환한다.
1. 데코레이터 함수에서 래퍼 함수을 반환한다.

원래 함수를 호출하기 전과 호출한 후에 메시지를 출력하는 함수 데코레이터의 예를 보겠다. 이 데코레이터를 `print_message`라고 하자.

```python
# Define the decorator function
def print_message(func):
    # Define the wrapper function
    def wrapper(*args, **kwargs):
        # Print a message before calling the original function
        print("Calling", func.__name__)
        # Call the original function and store its result
        result = func(*args, **kwargs)
        # Print a message after calling the original function
        print("Done", func.__name__)
        # Return the result of the original function
        return result
    # Return the wrapper function
    return wrapper
```

이제 이 데코레이터를 `@` 기호에 이어 데코레이터 함수의 이름을 사용하여 원하는 함수에 적용할 수 있다. 예를 들어, 어떤 수의 계승을 계산하는 함수에 적용해 보겠다.

```python
# Import the math module
import math
# Apply the decorator to the factorial function
@print_message
def factorial(n):
    # Return the factorial of n
    return math.factorial(n)
# Call the factorial function
factorial(5)
```

다음과 같은 출력을 얻을 수 있다.

```
Calling factorial
Done factorial
120
```

보다시피 데코레이터 함수는 원래 함수 자체를 변경하지 않고 원래 함수을 호출하기 전과 호출한 후에 메시지를 인쇄하는 기능이 추가되었다.

[다음 절](#sec_04)에서는 Python에서 클래스 데코레이터를 작성하고 사용하는 방법에 대해 알아보겠다.

## <a name="sec_04"></a> 클래스 데코레이터 작성과 사용 방법
클래스 데코레이터는 클래스를 인수로 받아 수정된 클래스를 반환하는 함수이다. 수정된 클래스는 메서드, 속성 또는 동작을 추가하거나 변경하는 등의 추가 기능이나 변경된 기능을 가질 수 있다. 원래 클래스는 변경되지 않았지만 데코레이터는 클래스의 인스턴스를 만들 때마다 수정 사항을 적용할 수 있도록 한다.

클래스 데코레이터를 작성하려면 다음 단계를 따라야 한다.

1. 원래 클래스를 상속하고 추가 또는 변경된 기능을 수행하는 래퍼 클래스를 정의한다.
1. 데코레이터 함수에서 래퍼 클래스를 반환한다.

원래 클래스에 메소드를 추가하는 클래스 데코레이터의 예를 보겠다. 이 데코레이터를 `add_method`라고 하자.

```python
# Define the decorator function
def add_method(func):
    # Define the wrapper class
    class Wrapper:
        # Inherit from the original class
        def __init__(self, *args, **kwargs):
            self.wrapped = func(*args, **kwargs)
        # Define the new method
        def new_method(self):
            # Do something
            print("This is a new method")
        # Delegate the other methods to the original class
        def __getattr__(self, name):
            return getattr(self.wrapped, name)
    # Return the wrapper class
    return Wrapper
```

이제 이 데코레이터를 `@` 기호와 데코레이터 함수의 이름을 사용하여 원하는 클래스에 적용할 수 있다. 예를 들어, 사람을 나타내는 클래스에 적용해 보겠다.

```python
# Apply the decorator to the Person class
@add_method
class Person:
    # Define the constructor
    def __init__(self, name, age):
        self.name = name
        self.age = age
    # Define a method
    def greet(self):
        print("Hello, I am", self.name)
# Create an instance of the Person class
p = Person("Alice", 25)
# Call the original method
p.greet()
# Call the new method
p.new_method()
```

다음과 같은 출력을 얻을 수 있다.

```
Hello, I am Alice
This is a new method
```

보다시피 데코레이터 함수는 원래 클래스 자체를 변경하지 않고 기존 클래스에 새로운 방식을 추가했다.

[다음 절](#sec_05)에서는 Python 프로그래밍에서 데코레이터의 일반적인 사용 사례와 예를 들 것이다.

## <a name="sec_05"></a> 데코레이터의 일반적인 사용 사례와 예
이 절에서는 Python 프로그래밍에서 데코레이터의 일반적인 사용 사례와 예를 보인다. 데코레이터는 다음과 같은 다양한 특징과 기능을 구현하는 데 사용될 수 있다.

- 로깅: 데코레이터는 함수 또는 클래스 메소드의 입력, 출력 및 실행 시간을 기록하는 데 사용할 수 있다. 이는 디버깅, 테스트 또는 성능 분석에 유용할 수 있다.
- 캐싱: 데코레이터는 계산 비용이 많이 들거나 자주 호출되는 함수 또는 클래스 메서드의 결과를 캐시하는 데 사용할 수 있다. 이는 프로그램의 효율성과 속도를 향상시킬 수 있다.
- Validation(검증): 함수 또는 클래스 메서드의 입력 또는 출력을 검증하는 데 데코레이터를 사용할 수 있다. 이를 통해 잘못된 데이터로 인해 오류나 예외가 발생하는 것을 방지할 수 있다.
- 권한 부여: 데코레이터는 함수 또는 클래스 메서드를 실행하기 전에 사용자의 권한 또는 인증 여부를 확인하는 데 사용할 수 있다. 이를 통해 프로그램의 보안과 개인 정보 보호를 강화할 수 있다.

이러한 사용 사례에 데코레이터를 작성하고 사용하는 방법에 대한 예를 살펴보겠다.

### 로깅
함수나 클래스 메소드의 입력, 출력, 실행 시간을 기록하는 데코레이터를 작성하고자 한다고 가정해 보겠다. 시간 모듈을 사용하여 시간을 측정하고 `logging` 모듈을 사용하여 정보를 기록할 수 있다. 또한 `functools` 모듈을 사용하여 원래 함수나 메소드의 메타데이터를 보존할 수도 있다. 이러한 데코레이터를 작성하는 방법은 다음과 같다.

```python
# Import the modules
import time
import logging
import functools
# Define the decorator function
def log(func):
    # Preserve the metadata of the original function or method
    @functools.wraps(func)
    # Define the wrapper function
    def wrapper(*args, **kwargs):
        # Get the current time
        start = time.time()
        # Call the original function or method and store its result
        result = func(*args, **kwargs)
        # Get the elapsed time
        end = time.time()
        # Calculate the execution time
        duration = end - start
        # Log the input, output, and execution time
        logging.info(f"Input: {args}, {kwargs}")
        logging.info(f"Output: {result}")
        logging.info(f"Execution time: {duration} seconds")
        # Return the result of the original function or method
        return result
    # Return the wrapper function
    return wrapper
```

이제 이 데코레이터를 `@` 기호와 데코레이터 함수의 이름을 사용하여 원하는 함수나 클래스 메서드에 적용할 수 있다. 예를 들어, 두 수의 합을 계산하는 함수에 적용해 보겠다.

```python
# Apply the decorator to the sum function
@log
def sum(a, b):
    # Return the sum of a and b
    return a + b
# Call the sum function
sum(3, 5)
```

이렇게 하면 로그 파일에 다음과 같은 출력이 기록된다.

```
INFO:root:Input: (3, 5), {}
INFO:root:Output: 8
INFO:root:Execution time: 0.000123456789 seconds
```

보다시피 데코레이터 함수는 원래 함수 자체를 변경하지 않고 원래 기능의 입력, 출력, 실행 시간을 기록했다.

### 캐싱
계산 비용이 많이 들거나 자주 호출되는 함수나 클래스 메서드의 결과를 캐시하는 데코레이터를 작성하고자 한다고 가정해 보겠다. `functools` 모듈을 사용하여 이전 호출의 결과를 저장하는 캐시 객체를 만들 수 있다. `functools` 모듈을 사용하여 원래 함수나 메서드의 메타데이터를 보존할 수도 있다. 이러한 데코레이터를 작성하는 방법은 다음과 같다.

```python
# Import the module
import functools
# Define the decorator function
def cache(func):
    # Create a cache object
    cache = functools.lru_cache(maxsize=None)
    # Preserve the metadata of the original function or method
    @functools.wraps(func)
    # Define the wrapper function
    def wrapper(*args, **kwargs):
        # Call the cache object with the original function or method and the arguments
        return cache(func, *args, **kwargs)
    # Return the wrapper function
    return wrapper
```

이제 이 데코레이터를 `@` 기호와 데코레이터 함수의 이름을 사용하여 원하는 함수나 클래스 메서드에 적용할 수 있다. 예를 들어 주어진 인덱스의 피보나치 수를 계산하는 함수에 적용해 보겠다.

```python
# Apply the decorator to the fibonacci function
@cache
def fibonacci(n):
    # Return the Fibonacci number of n
    if n < 2:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)
# Call the fibonacci function
fibonacci(10)
```

이렇게 하면 다음과 같이 출력된다.

```
55
```

보다시피 데코레이터 함수는 원래 함수 자체를 변경하지 않고 원래 함수의 결과를 캐싱했다. 이는 특히 재귀 함수의 경우 프로그램의 효율성과 속도를 향상시킬 수 있다.

### 검증
함수나 클래스 메소드의 입력 또는 출력을 검증하는 데코레이터를 작성하고자 한다고 가정해 보겠다. `functools` 모듈을 사용하여 원래 함수나 메소드의 메타데이터를 보존할 수 있다. 또한 `assert` 문을 사용하여 데이터의 유효성을 검사하고 조건이 충족되지 않으면 예외를 제기할 수 있다. 이러한 데코레이터를 작성하는 방법은 다음과 같다.

```python
# Import the module
import functools
# Define the decorator function
def validate(func):
    # Preserve the metadata of the original function or method
    @functools.wraps(func)
    # Define the wrapper function
    def wrapper(*args, **kwargs):
        # Validate the input
        assert all(isinstance(arg, int) for arg in args), "Input must be integers"
        # Call the original function or method and store its result
        result = func(*args, **kwargs)
        # Validate the output
        assert isinstance(result, int), "Output must be an integer"
        # Return the result of the original function or method
        return result
    # Return the wrapper function
    return wrapper
```

이제 이 데코레이터를 `@` 기호와 데코레이터 함수의 이름을 사용하여 원하는 함수나 클래스 메서드에 적용할 수 있다. 예를 들어, 두 수의 곱을 계산하는 함수에 적용해 보겠다.

```python
# Apply the decorator to the product function
@validate
def product(a, b):
    # Return the product of a and b
    return a * b
# Call the product function with valid input
product(3, 5)
```

이렇게 하면 다음과 같은 출력을 얻을 수 있다.

```
15
```

그러나 문자열이나 실수와 같이 입력이 잘못된 `product` 함수를 호출하면 데코레이터 함수는 예외를 발생시킨다.

```
# Call the product function with invalid input
product("3", 5)
```

이렇게 하면 다음과 같은 출력을 얻을 수 있다.

```
Traceback (most recent call last):
  File "", line 1, in 
  File "", line 6, in wrapper
AssertionError: Input must be integers
```

보다시피 데코레이터 함수는 원래 함수 자체를 변경하지 않고 원래 기능의 입력과 출력을 검증했다. 이를 통해 잘못된 데이터로 인한 오류나 예외 발생을 방지할 수 있다.

### 권한 부여
함수나 클래스 메서드를 실행하기 전에 사용자의 권한이나 인증 여부를 확인하는 데코레이터를 작성하고자 한다고 가정해 보자. `functools` 모듈을 사용하여 원래 함수나 메서드의 메타데이터를 보존할 수 있다. `getpass` 모듈을 사용하여 사용자의 사용자 이름과 암호를 가져와 저장된 자격 증명과 비교할 수도 있다. 이러한 데코레이터를 작성하는 방법은 다음과 같다.

```python
# Import the modules
import functools
import getpass

# Define the decorator function
def authorize(func):
    # Preserve the metadata of the original function or method
    @functools.wraps(func)
    # Define the wrapper function
    def wrapper(*args, **kwargs):
        # Ask for username and password
        username = input("Enter your username: ")
        password = getpass.getpass("Enter your password: ")

        # Check the credentials (this is a basic example, in a real scenario, you should check against a database or a secure storage)
        if username == "admin" and password == "admin":
            # If credentials are correct, call the original function or method
            return func(*args, **kwargs)
        else:
            # If credentials are incorrect, raise an exception
            raise PermissionError("You are not authorized to access this function")

    # Return the wrapper function
    return wrapper

# Example usage:
# Define a function that requires authorization
@authorize
def admin_function():
    return "You have accessed the admin function"

# Call the function
try:
    print(admin_function())
except PermissionError as e:
    print(e)
```

## <a name="summary"></a> 요약
이 포스팅에서 학습한 내용을 요약하면 다음과 같다.

- Python에서 데코레이터란 무엇이며 어떻게 작동하는가
- 함수의 동작을 수정하거나 향상시키기 위해 함수 데코레이터를 작성하고 사용하는 방법
- 수업 데코레이터를 작성하고 사용하여 수업의 행동을 수정하거나 향상시키는 방법
- 로깅, 캐싱, 검증 및 권한 부여와 같은 Python 프로그래밍에서 데코레이터의 몇 일반적인 사용 사례와 예

이제 Python에서 데코레이터의 개념과 적용에 대해 확실하게 이해하였을 것이다.
