# Python 데코레이터: 함수와 클래스 데코레이터 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 함수나 클래스의 동작을 수정하거나 향상시키기 위해 데코레이터를 사용하는 방법을 알아본다.</font>

## 목차

1. [개요](./decorators.md#intro)
1. [Python에서 데코레이터란?](./decorators.md#sec_02)
1. [함수 데코레이터 작성과 사용 방법](./decorators.md#sec_03)
1. [클래스 데코레이터 작성과 사용 방법](./decorators.md#sec_04)
1. [데코레이터의 일반적인 사용 사례와 예](./decorators.md#sec_05)
1. [요약](./decorators.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 28 — Python Decorators: Function and Class Decorators](https://medium.com/gitconnected/python-tutorial-28-python-decorators-function-and-class-decorators-f77cf5d0ef24?sk=33cb1caf8107dca894af74d7f8b09641)를 편역하였다.
